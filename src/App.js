import React from 'react';
import './styles/bootstrap.min.css';
import './App.css';
import './css/all.css';
import Header from './components/header';
import TypePizza from './components/typePizza';
import Pizza from "./components/pizza"
import TitlePage from './parts/pageTitle'

function App() {
  return (
    <body className="d-flex justify-content-center ">    
    <div className="App container">

        <Header />


        <TypePizza />
        <TitlePage/>
        <div className="pizzaItems row col-lg-12">
            <Pizza name="Чизбургер-пицца" 
            defaultTypeDough="традиционное" 
            doughTypes={["тонкое", "традиционное"]} 
            defaultTypeSize="30см" 
            sizePizza={["26см", "30см", "40см"]}
            price = "395"
            />
            
        </div>
        {/* sidebar */}

        
    </div>
  </body>
  );
}

export default App;
