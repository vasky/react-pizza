import React, {Component} from 'react'
import logotype from '../images/logo.png';
import './header.css'

export default class header extends Component{
    render(){
        return(
            <header>
        <div className="header  d-flex row justify-content-between align-items-center">
          <a href="/" className="">
          <div className="logoDiv noDecoration d-flex col-lg-4 col-md-4 col-sm-5  col-xs-4">
              <img src={logotype} alt="logo"></img>
              <div className="textDivLogo d-flex align-items-center">
                <div className="textLogo ">
                  <div className="titleLogo noDecoration">
                    REACT PIZZA
                  </div>
                  <div className="smalltextlogo">
                    Самая вкусная пицца во Вселенной!
                  </div> 
                </div>
              </div>  
          </div>
        </a> 
          <div className="infoCart d-flex col-md-2 col-lg-2 col-sm-3 col-xs-4 align-items-center justify-content-center ">
            <div className="price d-flex">
              520
              <div className="imgRuble">
                <i className="fas fa-ruble-sign"></i>
              </div>
            </div>
            <div className="line">

            </div>
            <div className="amountInCart d-flex">
              <div className="imgCart">
                <i className="fas fa-shopping-cart"></i>
              </div>
              2
            </div>
          </div>
        </div>
        <hr></hr>
      </header>
        )
    }
} 