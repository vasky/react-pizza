import React, {Component} from 'react'
import pizza from "../images/pizzas/01.png"
import './pizza.css'

export default class Pizza extends Component{

    constructor (props){
        super(props)
        this.state = {
            defaultTypeDough : this.props.defaultTypeDough,
            defaultPizzaSize : this.props.defaultTypeSize,
            amount: 0
        }
    }
    
    setDoughPizza(type){
        this.setState ({
            defaultTypeDough : type
        })
    }
    setSizePizza(type){
        this.setState ({
            defaultPizzaSize : type
        })
    }

    addToCart(){
        this.setState ({
            amount: this.state.amount + 1
        })
    }

    removeFromCart(){
        this.setState ({
            amount: this.state.amount -1
        })
    }

    

    render(props){
        return(
            <div className="pizzaItem col-lg-3 col-md-4 col-sm-6 col-xs-6">
                <div className="bodyCardPizza">
                <div className="productImg">
                    <img src={pizza} alt="пицца"></img>
                </div>
                <div className="pizzaName d-flex justify-content-center">
                    <h4>{this.props.name}</h4>
                </div>
                <div className="customizePizza justify-content-center d-flex justify-content-center">
                    <div className="propertiesPizza">
                    <div className="doughTypes d-flex justify-content-center">
                            {  this.props.doughTypes.map((type, index) =>{
                                if (type == this.state.defaultTypeDough)  {
                                    return(
                                        <button onClick={this.setDoughPizza.bind(this, type)} style={{background: "none",	color: "inherit",	border: "none",	padding: 0,	font: "inherit",	cursor: "pointer",	outline: "inherit"}} >                                
                                        <div className="doughType activeType pizzaProp" >                                         
                                            {type}
                                        </div>
                                        </button>
                                    )
                                }else{
                                    return(
                                        <button onClick={this.setDoughPizza.bind(this, type)} style={{background: "none",	color: "inherit",	border: "none",	padding: 0,	font: "inherit",	cursor: "pointer",	outline: "inherit"}} >                                
                                        <div className="doughType anactiveType pizzaProp" >                                         
                                            {type}
                                        </div>
                                        </button>
                                    )
                                }
                            }) } 
                                
                            
                        
                        {/* <div className="doughType activeType pizzaProp">
                            традиционное
                        </div> */}
                    </div>
                    <div className="sizesPizza d-flex justify-content-center">

                    {  this.props.sizePizza.map((type) =>{
                                if (type == this.state.defaultPizzaSize)  {
                                    return(
                                        <div className="sizePizza activeType pizzaProp" onClick={this.setSizePizza.bind(this, type)}>
                                            {type}
                                        </div>
                                    )
                                }else{
                                    return(
                                         <div className="sizePizza anactiveType pizzaProp" onClick={this.setSizePizza.bind(this, type)}>
                                            {type}
                                        </div>
                                    )
                                }
                            }) } 
                    </div>
                  </div>
                </div>
                <div className="downPizzaCartPart d-flex align-items-center justify-content-center">
                    <div className="pizzaPrice d-flex align-items-center justify-content-center">
                        от {this.props.price}
                        <i className="priceIconRuble fas fa-ruble-sign"></i>
                    </div>
                {/* <div className="buttonAddToCart d-flex justify-content-center">
                        <div className="addButton">
                            <i className="fas fa-plus"></i> Добавить
                        </div>
                    </div> */}
                    { this.state.amount >= 1?
                    <div className="deleteOrIcrease">
                    <i class="fas fa-minus" onClick={this.removeFromCart.bind(this)}></i>   
                        {this.state.amount}    
                    <i className="fas fa-plus fa-plus2" onClick={this.addToCart.bind(this)}></i>
                    </div>
                    : 
                    <div className="buttonAddToCart d-flex justify-content-center" onClick={this.addToCart.bind(this)}>
                            <div className="addButton">
                                <i className="fas fa-plus"></i> Добавить
                            </div>
                        </div>}
                </div>    
            </div>
        </div>
        )
    }
}