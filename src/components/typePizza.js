import React, {Component} from 'react'
import "./typePizza.css"
import Pizza from "./pizza"

export default class TypePizza extends Component {
    render(){
        return(
            <div className="content row col-lg-12 d-flex justify-content-between">
          <div className="categories  d-flex row col">
            <div className="categoryPizza d-flex justify-content-center align-items-center">
              Все
            </div>
            <div className="categoryPizza d-flex justify-content-center align-items-center">
              Мясные
            </div>
            <div className="categoryPizza d-flex justify-content-center align-items-center">
              Вегетарианские
            </div>
            <div className="categoryPizza d-flex justify-content-center align-items-center">
              Грибные
            </div>
          </div>

          <div className="sort d-flex col-lg-5 col-md-5 col-sm-12 justify-content-between">
          <div className="staticText d-flex align-items-center">
            <div className="arrowSort">
              <i className="fas fa-caret-up"></i>
            </div>
              Сортировка по: 
            </div>
            <div className="typeSort">
              <div className="typeSortText">
               популярности
              </div>
               <div className="test"></div>
            </div>
            
          </div>
        </div>
        )
    }
}