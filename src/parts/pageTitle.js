import React, {Component} from 'react'
import './parts.css'

export default class TitlePage extends Component{
    render(){
        return(
            <div className="titlePage row col-lg-12 d-flex justify-content-left"><h1>Все пиццы</h1></div>
        )
    }
}